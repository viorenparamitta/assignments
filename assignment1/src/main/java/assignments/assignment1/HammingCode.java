package assignments.assignment1;

import java.util.Scanner;

/**
 * @param xcode  code yang telah disisipkan x di dalamnya
 * @param parity jumlah redundan / bit parity
 * @return hamming code
 */
public class HammingCode {
    static final int ENCODE_NUM = 1;
    static final int DECODE_NUM = 2;
    static final int EXIT_NUM = 3;

    public static boolean duapangkat(int n) {
        while (n > 1) {
            if (n % 2 == 1) { // ini berarti bilangan ganjil
                return false;
            } else {
                n = n / 2; // n bisa dibagi 2 terus berarti 2 pangkat
            }
        }
        return true;
    }

    /**
     * @param xcode  code yang telah disisipkan x di dalamnya
     * @param parity jumlah redundan / bit parity
     * @return hamming code
     */
    public static String encode(String data) {
        int m = data.length();
        int r = 0;
        for (r = 0; r < m + 2; r++) {
            if (Math.pow(2.0, r) >= m + r + 1) {
                break; // berhenti looping lalu simpen nilai r nya
            }
        }

        int[] hamming = new int[m + r];
        for (int i = 0, j = 0; i < (m + r); i++) {
            if (duapangkat(i + 1)) { // kalau parity di skip
                continue;
            } else {
                hamming[i] = data.charAt(j++) - '0'; // masukin ke array
            }
        }

        // cek paritynya genap gak
        for (int i = 0; i < r; i++) {
            int sum = 0;
            for (int j = 0; j < (m + r); j++) {
                if (((j + 1) & (1 << i)) > 0) {
                    sum += hamming[j];
                }
            }
            hamming[(1 << i) - 1] = sum % 2;
        }

        String result = "";
        for (int i = 0; i < hamming.length; i++) {
            result += hamming[i]; // jadiin ke string
        }
        return result;
    }

    /**
     * @param xcode  code yang telah disisipkan x di dalamnya
     * @param parity jumlah redundan / bit parity
     * @return hamming code
     */
    public static String decode(String code) {
        int mr = code.length();
        int r = 0;
        for (r = 0; r < mr; r++) {
            if (Math.pow(2.0, r) >= mr + 1) {
                break; // berhenti looping lalu simpen nilai r nya
            }
        }

        // masukin hamming code ke dalam array
        int[] binary = new int[mr];
        for (int i = 0, j = 0; i < (mr); i++) {
            binary[i] = code.charAt(j++) - '0';
        }

        // nested loop untuk tau apakah parity sudah genap apa belum
        int error = 0;
        for (int i = 0; i < r; i++) {
            int sum = 0;
            for (int j = 0; j < (mr); j++) {
                if (((j + 1) & (1 << i)) > 0) {
                    sum += binary[j];
                }
            }

            if (sum % 2 != 0) {
                error += (int) Math.pow(2, i);
            }
        }

        // mengganti yang salah
        if (error != 0) {
            if (binary[error - 1] == (1)) {
                binary[error - 1] = 0;
            } else {
                binary[error - 1] = 1;
            }
        }

        // mengubah array menjadi string biasa
        String result = "";
        int paritybit = 0;
        for (int i = 0; i < binary.length; i++) {
            if (i != Math.pow(2, paritybit) - 1) {
                result += binary[i];
            } else {
                paritybit++;
            }
        }
        return result;
    }

    /**
     * Main program for Hamming Code.
     * 
     * @param args unused
     */
    public static void main(String[] args) {
        System.out.println("Selamat datang di program Hamming Code!");
        System.out.println("=======================================");
        Scanner in = new Scanner(System.in);
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println();
            System.out.println("Pilih operasi:");
            System.out.println("1. Encode");
            System.out.println("2. Decode");
            System.out.println("3. Exit");
            System.out.println("Masukkan nomor operasi yang diinginkan: ");
            int operation = in.nextInt();
            if (operation == ENCODE_NUM) {
                System.out.println("Masukkan data: ");
                String data = in.next();
                String code = encode(data);
                System.out.println("Code dari data tersebut adalah: " + code);
            } else if (operation == DECODE_NUM) {
                System.out.println("Masukkan code: ");
                String code = in.next();
                String data = decode(code);
                System.out.println("Data dari code tersebut adalah: " + data);
            } else if (operation == EXIT_NUM) {
                System.out.println("Sampai jumpa!");
                hasChosenExit = true;
            }
        }
        in.close();
    }
}
