package assignments.assignment3;

public class Pintu extends Benda {
    // TODO: Implementasikan abstract method yang terdapat pada class Benda
    // UNTUK MENAMBAH PERSENTASE
    public void tambahPersentase() {
        setPersentaseMenular(getPersentaseMenular() + 30);
    }

    public Pintu(String name) {
        // TODO: Implementasikan apabila objek CleaningService ini membersihkan benda
        // Hint: Update nilai atribut jumlahDibersihkan
        super(name);
    }

    public String toString() {
        return "PINTU " + getNama();
    }
}