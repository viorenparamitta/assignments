package assignments.assignment3;

public class TombolLift extends Benda {
    // TODO: Implementasikan abstract method yang terdapat pada class Benda
    // UNTUK MENAMBAH PERSENTASE
    public void tambahPersentase() {
        setPersentaseMenular(getPersentaseMenular() + 20);
    }

    public TombolLift(String name) {
        // TODO: Buat constructor untuk Tombol lift.
        // Hint: Akses constructor superclass-nya
        super(name);
    }

    public String toString() {
        return "TOMBOL LIFT " + getNama();
    }
}