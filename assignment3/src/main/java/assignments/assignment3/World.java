package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class World {

    public List<Carrier> listCarrier;

    public World() {
        // TODO: Buat constructor untuk class World
        this.listCarrier = new ArrayList<Carrier>();
    }

    public Carrier createObject(String tipe, String nama) {
        // TODO: Implementasikan apabila ingin membuat object sesuai dengan parameter
        // yang diberikan
        if (tipe.equalsIgnoreCase("ANGKUTAN_UMUM")) {
            Carrier object = new AngkutanUmum(nama);
            return object;
        } else if (tipe.equalsIgnoreCase("PEGANGAN_TANGGA")) {
            Carrier object = new PeganganTangga(nama);
            return object;
        } else if (tipe.equalsIgnoreCase("PINTU")) {
            Carrier object = new Pintu(nama);
            return object;
        } else if (tipe.equalsIgnoreCase("TOMBOL_LIFT")) {
            Carrier object = new TombolLift(nama);
            return object;
        } else if (tipe.equalsIgnoreCase("CLEANING_SERVICE")) {
            Carrier object = new CleaningService(nama);
            return object;
        } else if (tipe.equalsIgnoreCase("JURNALIS")) {
            Carrier object = new Jurnalis(nama);
            return object;
        } else if (tipe.equalsIgnoreCase("OJOL")) {
            Carrier object = new Ojol(nama);
            return object;
        } else if (tipe.equalsIgnoreCase("PEKERJA_JASA")) {
            Carrier object = new PekerjaJasa(nama);
            return object;
        } else if (tipe.equalsIgnoreCase("PETUGAS_MEDIS")) {
            Carrier object = new PetugasMedis(nama);
            return object;
        }
        return null;
    }

    public Carrier getCarrier(String nama) {
        // TODO: Implementasikan apabila ingin mengambil object carrier dengan nama
        // sesuai dengan parameter
        for (Carrier object : listCarrier) {
            if (object != null) {
                if (object.getNama().equalsIgnoreCase(nama)) {
                    return object;
                }
            } else
                continue;
        }
        return null;
    }
}
