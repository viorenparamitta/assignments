package assignments.assignment3;

public class PetugasMedis extends Manusia {

    private int jumlahDisembuhkan;

    public PetugasMedis(String nama) {
        // TODO: Buat constructor untuk Petugas Medis.
        // Hint: Akses constructor superclass-nya
        super(nama);
    }

    public String toString() {
        return "PETUGAS MEDIS " + getNama();
    }

    public void obati(Manusia manusia) {
        // TODO: Implementasikan apabila objek PetugasMedis ini menyembuhkan manusia
        // Hint: Update nilai atribut jumlahDisembuhkan
        manusia.ubahStatus("positif");
        this.jumlahDisembuhkan++;
        tambahSembuh();
        for (Carrier x : manusia.getRantaiPenular()) {
            x.setAktifKasusDisebabkan(x.getAktifKasusDisebabkan() - 1);
        }
        manusia.getRantaiPenular().clear();
    }

    public int getJumlahDisembuhkan() {
        // TODO: Kembalikan nilai dari atribut jumlahDisembuhkan
        return this.jumlahDisembuhkan;
    }
}