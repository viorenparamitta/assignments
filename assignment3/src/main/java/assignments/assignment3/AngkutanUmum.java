package assignments.assignment3;

public class AngkutanUmum extends Benda {
    // TODO: Implementasikan abstract method yang terdapat pada class Benda
    // UNTUK MENAMBAH PERSENTASE
    public void tambahPersentase() {
        setPersentaseMenular(getPersentaseMenular() + 35);
    }

    public AngkutanUmum(String name) {
        // TODO: Buat constructor untuk AngkutanUmum.
        // Hint: Akses constructor superclass-nya
        super(name);
    }

    public String toString() {
        return "ANGKUTAN UMUM " + getNama();
    }
}