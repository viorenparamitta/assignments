package assignments.assignment3;

public class PekerjaJasa extends Manusia {

    public PekerjaJasa(String nama) {
        // TODO: Buat constructor untuk Pekerja Jasa.
        // Hint: Akses constructor superclass-nya
        super(nama);
    }

    public String toString() {
        return "PEKERJA JASA " + getNama();
    }

}