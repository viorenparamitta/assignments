package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public abstract class Carrier {

    private String nama;
    private String tipe;
    private Status statusCovid;
    private int aktifKasusDisebabkan;
    private int totalKasusDisebabkan;
    private List<Carrier> rantaiPenular;

    public Carrier(String nama, String tipe) {
        // TODO: Buat constructor untuk Carrier.
        this.nama = nama;
        this.tipe = tipe;
        this.rantaiPenular = new ArrayList<Carrier>();
        this.statusCovid = new Negatif();
    }

    public String getNama() {
        // TODO : Kembalikan nilai dari atribut nama
        return this.nama;
    }

    public String getTipe() {
        // TODO : Kembalikan nilai dari atribut tipe
        return this.tipe;
    }

    public String getStatusCovid() {
        // TODO : Kembalikan nilai dari atribut statusCovid
        return this.statusCovid.getStatus();
    }

    // MEMBUAT SETTER UNTUK AKTIF KASUS
    public void setAktifKasusDisebabkan(int aktifKasus) {
        this.aktifKasusDisebabkan = aktifKasus;
    }

    public int getAktifKasusDisebabkan() {
        // TODO : Kembalikan nilai dari atribut aktifKasusDisebabkan
        return this.aktifKasusDisebabkan;
    }

    public int getTotalKasusDisebabkan() {
        // TODO : Kembalikan nilai dari atribut totalKasusDisebabkan
        return this.totalKasusDisebabkan;
    }

    public List<Carrier> getRantaiPenular() {
        // TODO : Kembalikan nilai dari atribut rantaiPenular
        return this.rantaiPenular;
    }

    public void ubahStatus(String status) {
        // TODO : Implementasikan fungsi ini untuk mengubah atribut dari statusCovid
        if (status.equalsIgnoreCase("positif")) {
            this.statusCovid = new Negatif();
        } else if (status.equalsIgnoreCase("negatif")) {
            this.statusCovid = new Positif();
        }
    }

    public void interaksi(Carrier lain) {
        // TODO : Objek ini berinteraksi dengan objek lain
        if (lain.getStatusCovid().equalsIgnoreCase("positif") && (this.getStatusCovid().equalsIgnoreCase("negatif"))) {
            lain.statusCovid.tularkan(lain, this); // LAIN MENULARKAN THIS
            if (this.getStatusCovid().equalsIgnoreCase("positif")) { // JIKA OBJECT THIS MJD POSITIF MAKA MASUK RANTAI
                                                                     // DAN JUMLAH TOTAL DAN AKTIF KASUS BERTAMBAH
                this.rantaiPenular = new ArrayList<>();
                this.rantaiPenular.addAll(lain.getRantaiPenular());
                this.rantaiPenular.add(lain);
                for (Carrier x : this.rantaiPenular) {
                    if (!this.equals(x)) {
                        x.totalKasusDisebabkan++;
                        x.aktifKasusDisebabkan++;
                    }
                }
            }
        } else if ((this.getStatusCovid().equalsIgnoreCase("positif"))
                && (lain.getStatusCovid().equalsIgnoreCase("negatif"))) {
            this.statusCovid.tularkan(this, lain);
            if (lain.getStatusCovid().equalsIgnoreCase("positif")) {// JIKA OBJECT LAIN MJD POSITIF MAKA MASUK RANTAI
                                                                    // DAN JUMLAH TOTAL DAN AKTIF KASUS BERTAMBAH
                lain.rantaiPenular = new ArrayList<>();
                lain.rantaiPenular.addAll(this.getRantaiPenular());
                lain.rantaiPenular.add(this);
                for (Carrier x : lain.rantaiPenular) {
                    if (!lain.equals(x)) {
                        x.totalKasusDisebabkan++;
                        x.aktifKasusDisebabkan++;
                    }
                }
            }
        } else if (this.getStatusCovid().equalsIgnoreCase("positif")
                && lain.getStatusCovid().equalsIgnoreCase("positif")) {

        } else if (this.getStatusCovid().equalsIgnoreCase("negatif")
                && lain.getStatusCovid().equalsIgnoreCase("negatif")) {
        }
    }

    public abstract String toString();

}
