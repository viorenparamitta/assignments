package assignments.assignment3;

public class CleaningService extends Manusia {

    private int jumlahDibersihkan;

    public CleaningService(String nama) {
        // TODO: Buat constructor untuk CleaningService.
        // Hint: Akses constructor superclass-nya
        super(nama);
    }

    public String toString() {
        return "CLEANING SERVICE " + getNama();
    }

    public void bersihkan(Benda benda) {
        // TODO: Implementasikan apabila objek CleaningService ini membersihkan benda
        // Hint: Update nilai atribut jumlahDibersihkan
        benda.setPersentaseMenular(0);
        benda.ubahStatus("positif");
        this.jumlahDibersihkan++;
        for (Carrier x : benda.getRantaiPenular()) {
            x.setAktifKasusDisebabkan(x.getAktifKasusDisebabkan() - 1); 
        }
        benda.getRantaiPenular().clear();
    }

    public int getJumlahDibersihkan() {
        // TODO: Kembalikan nilai dari atribut jumlahDibersihkan
        return this.jumlahDibersihkan;
    }

}