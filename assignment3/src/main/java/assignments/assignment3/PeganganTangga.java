package assignments.assignment3;

public class PeganganTangga extends Benda {
    // TODO: Implementasikan abstract method yang terdapat pada class Benda
    // UNTUK MENAMBAH PERSENTASE
    public void tambahPersentase() {
        setPersentaseMenular(getPersentaseMenular() + 20);
    }

    public PeganganTangga(String name) {
        // TODO: Buat constructor untuk Pegangan Tangga.
        // Hint: Akses constructor superclass-nya
        super(name);
    }

    public String toString() {
        return "PEGANGAN TANGGA " + getNama();
    }

}