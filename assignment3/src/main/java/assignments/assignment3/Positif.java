package assignments.assignment3;

public class Positif implements Status {

    public String getStatus() {
        return "Positif";
    }

    public void tularkan(Carrier penular, Carrier tertular) {
        // TODO: Implementasikan apabila object Penular melakukan interaksi dengan
        // object tertular
        // Hint: Handle kasus ketika keduanya benda dapat dilakukan disini
        if ((penular instanceof Benda) && (tertular instanceof Manusia)) {
            if (penular.getStatusCovid().equalsIgnoreCase("positif")) {
                tertular.ubahStatus("negatif");
            }
        } else if ((penular instanceof Manusia) && (tertular instanceof Benda)) {
            if (penular.getStatusCovid().equalsIgnoreCase("positif")) {
                if (tertular instanceof AngkutanUmum) {
                    ((Benda) tertular).tambahPersentase();
                    if (((Benda) tertular).getPersentaseMenular() >= 100) {
                        tertular.ubahStatus("negatif");
                    }
                } else if (tertular instanceof PeganganTangga) {
                    ((Benda) tertular).tambahPersentase();
                    if (((Benda) tertular).getPersentaseMenular() >= 100) {
                        tertular.ubahStatus("negatif");
                    }
                } else if (tertular instanceof Pintu) {
                    ((Benda) tertular).tambahPersentase();
                    if (((Benda) tertular).getPersentaseMenular() >= 100) {
                        tertular.ubahStatus("negatif");
                    }
                } else if (tertular instanceof TombolLift) {
                    ((Benda) tertular).tambahPersentase();
                    if (((Benda) tertular).getPersentaseMenular() >= 100) {
                        tertular.ubahStatus("negatif");
                    }
                }
            }
        } else if ((penular instanceof Manusia) && (tertular instanceof Manusia)) {
            if (penular.getStatusCovid().equalsIgnoreCase("positif"))
                tertular.ubahStatus("negatif");
        } else if ((penular instanceof Benda) && (tertular instanceof Benda)) {

        }
    }
}