package assignments.assignment3;

import java.io.*;

public class InputOutput {

    private BufferedReader br;
    private PrintWriter pw;
    private String inputFile;
    private String outputFile;
    private World world;

    public InputOutput(String inputType, String inputFile, String outputType, String outputFile) {
        // TODO: Buat constructor untuk InputOutput.
        this.inputFile = inputFile;
        setBufferedReader(inputType);
        this.outputFile = outputFile;
        setPrintWriter(outputType);
        this.world = new World();
    }

    public void setBufferedReader(String inputType) {
        // TODO: Membuat BufferedReader bergantung inputType (I/O text atau input
        // terminal)
        if (inputType.equalsIgnoreCase("text")) {
            try {
                BufferedReader br = new BufferedReader(new FileReader(this.inputFile));
                this.br = br;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (inputType.equalsIgnoreCase("terminal")) {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            this.br = br;
        }
    }

    public void setPrintWriter(String outputType) {
        // TODO: Membuat PrintWriter bergantung inputType (I/O text atau output
        // terminal)
        if (outputType.equalsIgnoreCase("text")) {
            try {
                PrintWriter pw = new PrintWriter(new FileWriter(this.outputFile));
                this.pw = pw;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (outputType.equalsIgnoreCase("terminal")) {
            PrintWriter pw = new PrintWriter(System.out, true);
            this.pw = pw;
        }
    }

    public void run() throws IOException {
        // TODO: Program utama untuk InputOutput, jangan lupa handle untuk IOException
        // Hint: Buatlah objek dengan class World
        // Hint: Untuk membuat object Carrier baru dapat gunakan method CreateObject
        // pada class World
        // Hint: Untuk mengambil object Carrier dengan nama yang sesua dapat gunakan
        // method getCarrier pada class World
        this.inputFile = br.readLine();
        while (this.inputFile != null) {
            if (inputFile.length() <= 0) {
                inputFile = br.readLine();
                continue;
            }
            // MEMBUAT SEMUA QUERY YG DIBUTUHKAN
            String[] query = inputFile.split(" ");
            if (query[0].equalsIgnoreCase("ADD")) {
                Carrier objek1 = this.world.createObject(query[1], query[2]);
                if (!(this.world.listCarrier.contains(objek1))) {
                    this.world.listCarrier.add(objek1);
                }
                // pw.println(world.listCarrier);
            } else if (query[0].equalsIgnoreCase("INTERAKSI")) {
                Carrier objek1 = this.world.getCarrier(query[1]);
                Carrier objek2 = this.world.getCarrier(query[2]);
                if (this.world.listCarrier.contains(objek1) && this.world.listCarrier.contains(objek2)) {
                    objek1.interaksi(objek2);
                }
            } else if (query[0].equalsIgnoreCase("POSITIFKAN")) {
                Carrier objek1 = this.world.getCarrier(query[1]);
                if (this.world.listCarrier.contains(objek1)) {
                    objek1.ubahStatus("negatif");
                }
            } else if (query[0].equalsIgnoreCase("SEMBUHKAN")) {
                Carrier objek1 = this.world.getCarrier(query[1]);
                Carrier objek2 = this.world.getCarrier(query[2]);
                if (this.world.listCarrier.contains(objek1) && this.world.listCarrier.contains(objek2)) {
                    if (objek1 instanceof PetugasMedis && objek2 instanceof Manusia) {
                        if (objek2.getStatusCovid().equalsIgnoreCase("positif")) {
                            ((PetugasMedis) objek1).obati((Manusia) objek2);
                        }
                    }
                }
            } else if (query[0].equalsIgnoreCase("BERSIHKAN")) {
                Carrier objek1 = this.world.getCarrier(query[1]);
                Carrier objek2 = this.world.getCarrier(query[2]);
                if (this.world.listCarrier.contains(objek1) && this.world.listCarrier.contains(objek2)) {
                    if (objek1 instanceof CleaningService && objek2 instanceof Benda) {
                        if (((Benda) objek2).getPersentaseMenular() > 0) {
                            ((CleaningService) objek1).bersihkan((Benda) objek2);
                        }
                    }
                }
            } else if (query[0].equalsIgnoreCase("RANTAI")) {
                Carrier objek1 = this.world.getCarrier(query[1]);
                if (this.world.listCarrier.contains(objek1)) {
                    try {
                        if (objek1.getStatusCovid().equalsIgnoreCase("positif")) {
                            if (objek1.getRantaiPenular().size() == 0) {
                                pw.println(String.format("Rantai Penyebaran %s: %s", objek1.toString(),
                                        objek1.toString()));
                            } else {
                                pw.print(String.format("Rantai Penyebaran %s: ", objek1.toString()));
                                for (Carrier x : objek1.getRantaiPenular()) {
                                    pw.print(String.format("%s -> ", x.toString()));
                                }
                                pw.println(objek1.toString());
                            }
                        } else {
                            throw new BelumTertularException(String.format("%s berstatus negatif", objek1.toString()));
                        }
                    } catch (BelumTertularException e) {
                        pw.println(e.toString());
                    }
                }
            } else if (query[0].equalsIgnoreCase("TOTAL_KASUS_DARI_OBJEK")) {
                Carrier objek1 = this.world.getCarrier(query[1]);
                if (this.world.listCarrier.contains(objek1)) {
                    pw.println(String.format("%s telah menyebarkan virus COVID ke %d objek", objek1.toString(),
                            objek1.getTotalKasusDisebabkan()));
                }
            } else if (query[0].equalsIgnoreCase("AKTIF_KASUS_DARI_OBJEK")) {
                Carrier objek1 = this.world.getCarrier(query[1]);
                if (this.world.listCarrier.contains(objek1)) {
                    pw.println(String.format(
                            "%s telah menyebarkan virus COVID dan masih teridentifikasi positif sebanyak ke %d objek",
                            objek1.toString(), objek1.getAktifKasusDisebabkan()));
                }
            } else if (query[0].equalsIgnoreCase("TOTAL_SEMBUH_MANUSIA")) {
                pw.println(String.format("Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: %d kasus",
                        Manusia.getJumlahSembuh()));
            } else if (query[0].equalsIgnoreCase("TOTAL_SEMBUH_PETUGAS_MEDIS")) {
                Carrier objek1 = this.world.getCarrier(query[1]);
                if (this.world.listCarrier.contains(objek1)) {
                    pw.println(String.format("%s menyembuhkan %d manusia", objek1.toString(),
                            ((PetugasMedis) objek1).getJumlahDisembuhkan()));
                }
            } else if (query[0].equalsIgnoreCase("TOTAL_BERSIH_CLEANING_SERVICE")) {
                if (world.getCarrier(query[1]) instanceof CleaningService) {
                    Carrier objek1 = this.world.getCarrier(query[1]);
                    if (this.world.listCarrier.contains(objek1)) {
                        pw.println(String.format("%s membersihkan %d benda", objek1.toString(),
                                ((CleaningService) objek1).getJumlahDibersihkan()));
                    }
                }
            } else if (query[0].equalsIgnoreCase("EXIT")) {
                break;
            }
            pw.flush();
            this.inputFile = br.readLine();
        }
        this.br.close();
        this.pw.close();
    }
}