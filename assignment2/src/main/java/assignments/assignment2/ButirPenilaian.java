package assignments.assignment2;

public class ButirPenilaian {
    private static final int PENALTI_KETERLAMBATAN = 20;
    private double nilai;
    private boolean terlambat;

    public ButirPenilaian(double nilai, boolean terlambat) {
        // nilai harus >= 0
        if (nilai >= 0)
            this.nilai = nilai;
        else
            this.nilai = 0;
        this.terlambat = terlambat;
    }

    public double getNilai() {
        // get nilainya
        if (this.terlambat == true)
            return this.nilai - this.nilai * (PENALTI_KETERLAMBATAN * 0.01);
        else
            return this.nilai;
    }

    @Override
    public String toString() {
        // pemotongan nilai jika terlambat
        if (this.terlambat == true)
            return String.format("%.2f", getNilai()) + " (T)";
        else
            return String.format("%.2f", getNilai());
    }
}
