package assignments.assignment2;

public class Mahasiswa implements Comparable<Mahasiswa> {
    private String npm;
    private String nama;
    private KomponenPenilaian[] komponenPenilaian;

    public Mahasiswa(String npm, String nama, KomponenPenilaian[] komponenPenilaian) {
        // TODO: buat constructor untuk Mahasiswa.
        // Note: komponenPenilaian merupakan skema penilaian yang didapat dari GlasDOS.
        this.npm = npm;
        this.nama = nama;
        this.komponenPenilaian = komponenPenilaian;
    }

    public KomponenPenilaian getKomponenPenilaian(String namaKomponen) {
        // TODO: kembalikan KomponenPenilaian yang bernama namaKomponen.
        // Note: jika tidak ada, kembalikan null atau lempar sebuah Exception.
        for (KomponenPenilaian nama : komponenPenilaian) {
            if (nama.getNama().equalsIgnoreCase(namaKomponen))
                return nama;
        }
        return null;
    }

    public String getNpm() {
        // TODO: kembalikan NPM mahasiswa.
        return this.npm;
    }

    /**
     * Mengembalikan huruf berdasarkan nilai yang diberikan.
     * 
     * @param nilaiAkhir nilai untuk dicari hurufnya.
     * @return huruf dari nilai.
     */
    private static String getHuruf(double nilai) {
        return nilai >= 85 ? "A"
                : nilai >= 80 ? "A-"
                        : nilai >= 75 ? "B+"
                                : nilai >= 70 ? "B"
                                        : nilai >= 65 ? "B-"
                                                : nilai >= 60 ? "C+" : nilai >= 55 ? "C" : nilai >= 40 ? "D" : "E";
    }

    /**
     * Mengembalikan status kelulusan berdasarkan nilaiAkhir yang diberikan.
     * 
     * @param nilaiAkhir nilai akhir mahasiswa.
     * @return status kelulusan (LULUS/TIDAK LULUS).
     */
    private static String getKelulusan(double nilaiAkhir) {
        return nilaiAkhir >= 55 ? "LULUS" : "TIDAK LULUS";
    }

    public String rekap() {
        // TODO: kembalikan rekapan sesuai dengan permintaan soal.
        String rekapan = "";
        double nilai = 0;
        String na = "";
        for (KomponenPenilaian x : komponenPenilaian) {
            rekapan += x;
            nilai += x.getNilai();
        }
        na += String.format("Nilai Akhir: %.2f \n", nilai);
        return rekapan + na + ("Huruf: " + getHuruf(nilai) + "\n") + getKelulusan(nilai) + "\n";
    }

    public String toString() {
        // TODO: kembalikan representasi String dari Mahasiswa sesuai permintaan soal.
        return getNpm() + " - " + this.nama;
    }

    public String getDetail() {
        // TODO: kembalikan detail dari Mahasiswa sesuai permintaan soal.
        String detail = "";
        double nilainya = 0;
        String a = "";
        for (KomponenPenilaian x : komponenPenilaian) {
            detail += x.getDetail() + "\n";
            nilainya += x.getNilai();
        }
        a += String.format("Nilai Akhir: %.2f \n", nilainya);
        return detail + a + ("Huruf :" + getHuruf(nilainya) + "\n") + getKelulusan(nilainya);
    }

    @Override
    public int compareTo(Mahasiswa other) {
        // TODO: definisikan cara membandingkan seorang mahasiswa dengan mahasiswa
        // lainnya.
        // Hint: bandingkan NPM-nya, String juga punya method compareTo.
        return this.getNpm().compareTo(other.getNpm());
    }
}
