package assignments.assignment2;

import java.util.ArrayList;

public class KomponenPenilaian {
    private final String nama;
    private final ButirPenilaian[] butirPenilaian;
    private final int bobot;

    public KomponenPenilaian(final String nama, final int banyakButirPenilaian, final int bobot) {
        this.nama = nama;
        this.butirPenilaian = new ButirPenilaian[banyakButirPenilaian];
        this.bobot = bobot;
    }

    /**
     * Membuat objek KomponenPenilaian baru berdasarkan bentuk KomponenPenilaian
     * templat.
     * 
     * @param templat templat KomponenPenilaian.
     */
    private KomponenPenilaian(final KomponenPenilaian templat) {
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    /**
     * Mengembalikan salinan skema penilaian berdasarkan templat yang diberikan.
     * 
     * @param templat templat skema penilaian sebagai sumber.
     * @return objek baru yang menyerupai templat.
     */
    public static KomponenPenilaian[] salinTemplat(final KomponenPenilaian[] templat) {
        final KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for (int i = 0; i < salinan.length; i++) {
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }

    public void masukkanButirPenilaian(final int idx, final ButirPenilaian butir) {
        // TODO: masukkan butir ke butirPenilaian pada index ke-idx.
        butirPenilaian[idx] = butir;
    }

    public String getNama() {
        // TODO: kembalikan nama KomponenPenilaian.
        return this.nama;
    }

    public double getRerata() {
        // TODO: kembalikan rata-rata butirPenilaian.
        double counter = 0;
        double xnull = 0;
        for (final ButirPenilaian nilai : butirPenilaian) {
            if (nilai == null)
                continue;
            else {
                counter += nilai.getNilai();
                xnull++;
            }
        }
        if (xnull == 0)
            return 0;
        else
            return counter / xnull;
    }

    public double getNilai() {
        // TODO: kembalikan rerata yang sudah dikalikan dengan bobot.
        return getRerata() * (((double) this.bobot) / 100.0);
    }

    public String getDetail() {
        // TODO: kembalikan detail KomponenPenilaian sesuai permintaan soal.
        String isinya = "";
        final ArrayList<ButirPenilaian> butirgaknull = new ArrayList<>();
        isinya += String.format("~~~ %s (%d%%) ~~~ \n", this.nama, this.bobot);
        for (final ButirPenilaian x : butirPenilaian) {
            if (x != null) {
                butirgaknull.add(x);
            } else {
                continue;
            }
        }
        if (butirgaknull.size() == 1 && butirgaknull.get(0) == null) {
            isinya += String.format("%s: %.2f \n", getNama(), getRerata());

        } else if (butirgaknull.size() == 1) {
            isinya += String.format("%s: %s \n", getNama(), butirgaknull.get(0).toString());

        } else if (butirgaknull.size() > 1) {
            for (int i = 0; i < butirgaknull.size(); i++) {
                isinya += String.format("%s %d: %s \n", getNama(), i + 1, butirgaknull.get(i).toString());
            }
            isinya += String.format("Rerata: %.2f \n", getRerata());
        } else {
            isinya += String.format("Rerata: 0.00 \n");
        }

        isinya += String.format("Kontribusi nilai akhir: %.2f \n", getNilai());
        return isinya;
    }

    @Override
    public String toString() {
        return String.format("Rerata %s: %.2f \n", this.nama, this.getRerata());
    }

}
