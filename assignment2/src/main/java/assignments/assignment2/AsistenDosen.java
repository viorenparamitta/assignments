package assignments.assignment2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AsistenDosen {
    private List<Mahasiswa> mahasiswa = new ArrayList<>();
    private String kode;
    private String nama;

    public AsistenDosen(String kode, String nama) {
        this.kode = kode;
        this.nama = nama;
    }

    public String getKode() {
        return this.kode;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        // TODO: tambahkan mahasiswa ke dalam daftar mahasiswa dengan mempertahankan
        // urutan.
        // Hint: kamu boleh menggunakan Collections.sort atau melakukan sorting manual.
        // Note: manfaatkan method compareTo pada Mahasiswa.
        this.mahasiswa.add(mahasiswa);
        Collections.sort(this.mahasiswa);
    }

    public Mahasiswa getMahasiswa(String npm) {
        // TODO: kembalikan objek Mahasiswa dengan NPM tertentu dari daftar mahasiswa.
        // Note: jika tidak ada, kembalikan null atau lempar sebuah Exception.
        for (Mahasiswa x : mahasiswa) {
            if (x.getNpm().equalsIgnoreCase(npm)) {
                return x;
            }
        }
        return null;
    }

    public String rekap() {
        // Merekap seluruh mahasiswa yang menjadi tanggung jawabnya
        String rekaplagi = "";
        for (Mahasiswa a : mahasiswa) {
            rekaplagi += a.toString() + "\n";
            rekaplagi += a.rekap();
            rekaplagi += "\n";
        }
        return rekaplagi;
    }

    public String toString() {
        String a = String.format("%s - %s", this.kode, this.nama);
        return a;
    }
}
